package com.domain;

public class Health {

	private Boolean hypertension;
	private Boolean bloodPressure;
	private Boolean bloodSugar;
	private Boolean overweight;
	
	public Health(){}
	
	public Health(Boolean hypertension, Boolean bloodPressure,
			Boolean bloodSugar, Boolean overweight) {
		super();
		this.hypertension = hypertension;
		this.bloodPressure = bloodPressure;
		this.bloodSugar = bloodSugar;
		this.overweight = overweight;
	}

	public Boolean getHypertension() {
		return hypertension;
	}
	public void setHypertension(Boolean hypertension) {
		this.hypertension = hypertension;
	}
	public Boolean getBloodPressure() {
		return bloodPressure;
	}
	public void setBloodPressure(Boolean bloodPressure) {
		this.bloodPressure = bloodPressure;
	}
	public Boolean getBloodSugar() {
		return bloodSugar;
	}
	public void setBloodSugar(Boolean bloodSugar) {
		this.bloodSugar = bloodSugar;
	}
	public Boolean getOverweight() {
		return overweight;
	}
	public void setOverweight(Boolean overweight) {
		this.overweight = overweight;
	}

}
