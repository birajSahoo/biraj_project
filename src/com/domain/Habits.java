package com.domain;

public class Habits {

	private Boolean smoking;
	private Boolean alcohol;
	private Boolean dailyExercise;
	private Boolean drugs;
	public Habits(){}
	
	public Habits(Boolean smoking, Boolean alcohol, Boolean dailyExercise,
			Boolean drugs) {
		super();
		this.smoking = smoking;
		this.alcohol = alcohol;
		this.dailyExercise = dailyExercise;
		this.drugs = drugs;
	}
	
	public Boolean getSmoking() {
		return smoking;
	}
	public void setSmoking(Boolean smoking) {
		this.smoking = smoking;
	}
	public Boolean getAlcohol() {
		return alcohol;
	}
	public void setAlcohol(Boolean alcohol) {
		this.alcohol = alcohol;
	}
	public Boolean getDailyExercise() {
		return dailyExercise;
	}
	public void setDailyExercise(Boolean dailyExercise) {
		this.dailyExercise = dailyExercise;
	}
	public Boolean getDrugs() {
		return drugs;
	}
	public void setDrugs(Boolean drugs) {
		this.drugs = drugs;
	}
	
}
