package com.domain;

public class User {

	private String name;
	private Boolean gender;
	private Integer age;
	private Health health;
	private Habits habits;
	
	public User(){}
	
	public User(String name, Boolean gender, Integer age, Health health,
			Habits habits) {
		super();
		this.name = name;
		this.gender = gender;
		this.age = age;
		this.health = health;
		this.habits = habits;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getGender() {
		return gender;
	}
	public void setGender(Boolean gender) {
		this.gender = gender;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Health getHealth() {
		return health;
	}
	public void setHealth(Health health) {
		this.health = health;
	}
	public Habits getHabits() {
		return habits;
	}
	public void setHabits(Habits habits) {
		this.habits = habits;
	}
	
}
