package com.test;

import java.util.Scanner;

import com.domain.Habits;
import com.domain.Health;
import com.domain.User;
import com.service.AppService;
import com.service.AppServiceImpl;

public class BillCalculationTest {

	public static void main(String[] args) {
		AppService service = new AppServiceImpl();
		User user = new User();
		Health health = new Health();
		Habits habits = new Habits();
		Scanner s = new Scanner(System.in);
		try{
			System.out.println("Please enter your name.");
			user.setName(s.nextLine());
			System.out.println("Enter gender Male/Female");
			String gender= s.nextLine();
			if(gender.equalsIgnoreCase("Male"))
				user.setGender(true);
			else
				user.setGender(false);
			System.out.println("Please enter age in number.");
			user.setAge(s.nextInt());
			String temp = null;
			System.out.println("Hypertension(yes/No)");
			temp = s.next();
			if(temp.equalsIgnoreCase("yes")) 
				health.setHypertension(true);
			else
				health.setHypertension(false);
			System.out.println("Blood preassure(yes/no)");
			temp = s.next();
			if(temp.equalsIgnoreCase("yes")) 
				health.setBloodPressure(true);
			else
				health.setBloodPressure(false);
			System.out.println("Blood sugar(yes/no)");
			temp = s.next();
			if(temp.equalsIgnoreCase("yes")) 
				health.setBloodSugar(true);
			else
				health.setBloodSugar(false);
			System.out.println("Over weight(yes/no)");
			temp = s.next();
			if(temp.equalsIgnoreCase("yes")) 
				health.setOverweight(true);
			else
				health.setOverweight(false);
			user.setHealth(health);
			
			System.out.println("Alcohal(yes/no)");
			temp = s.next();
			if(temp.equalsIgnoreCase("yes")) 
				habits.setAlcohol(true);
			else
				habits.setAlcohol(false);
			System.out.println("Smoking(yes/no)");
			temp = s.next();
			if(temp.equalsIgnoreCase("yes")) 
				habits.setSmoking(true);
			else
				habits.setSmoking(false);
			System.out.println("Daily exercise(yes/no)");
			temp = s.next();
			if(temp.equalsIgnoreCase("yes")) 
				habits.setDailyExercise(true);
			else
				habits.setDailyExercise(false);
			System.out.println("Drugs(yes/no)");
			temp = s.next();
			if(temp.equalsIgnoreCase("yes")) 
				habits.setDrugs(true);
			else
				habits.setDrugs(false);
			user.setHabits(habits);
			
			System.out.println("Health Insurance Premium for Mr."+user.getName()+": "+service.calculateBill(user));
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Invalid Input");
		}
	}
}
