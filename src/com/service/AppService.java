package com.service;

import com.domain.User;

public interface AppService {

	public Integer calculateBill(User user) ;
}
