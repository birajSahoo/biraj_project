package com.service;

import com.domain.Habits;
import com.domain.Health;
import com.domain.User;

public class AppServiceImpl implements AppService {
	public static final Integer baseAmount=5000;

	@Override
	public Integer calculateBill(User user) {
		Integer bill = baseAmount;
		if(user.getAge() <18) {
			return bill;
		} else{
			bill = bill+(bill*10/100);
		}
		if(user.getAge()>=25) {
			bill = bill+(bill*10/100);
		}
		if(user.getAge()>=30) {
			bill = bill+(bill*10/100);
		}
		if(user.getAge()>=35) {
			bill = bill+(bill*10/100);
		}
		if(user.getAge()>=40) {
			Integer count = 0;
			if(user.getAge()%5 !=0) {
				count = (user.getAge()-40)/5 + 1;
			} else {
				count = (user.getAge()-40)/5;
			}
			for(int i=0;i<count;i++) {
				bill = calculateBillByAge(bill);
			}
		}
		if(user.getGender() != null) {
			if(user.getGender())
				bill = bill+(bill*2/100);
		}
		if(user.getHealth() != null)
			bill = calculateBillByHealth(user.getHealth(),bill);
		if(user.getHabits() != null)
			bill = calculateBillByHabits(user.getHabits(),bill);
		
		return bill;
	}
	
	public Integer calculateBillByAge(Integer bill) {
		bill = bill+(bill*20/100);
		return bill;
	}
	
	public Integer calculateBillByHealth(Health health,Integer bill) {
		int count=0;
		if(health.getBloodPressure())
			count++;
		if(health.getBloodSugar()) 
			count++;
		if(health.getOverweight())
			count++;
		if(health.getHypertension())
			count++;
		bill = bill+(bill*count/100);
		
		return bill;
	}
	
	public Integer calculateBillByHabits(Habits habits,Integer bill) {
		int count = 0;
		if(habits.getDailyExercise())
			count--;
		if(habits.getAlcohol())
			count++;
		if(habits.getDrugs())
			count++;
		if(habits.getSmoking())
			count++;
		if(count<0)
			bill = bill-(bill*3/100);
		else
			bill = bill+(bill*3*count/100);
		return bill;
	}

}
